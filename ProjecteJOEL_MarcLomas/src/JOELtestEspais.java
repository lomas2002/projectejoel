import static org.junit.Assert.*;
import java.util.Arrays;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)

public class JOELtestEspais {
    
    private String paraula;
    private boolean funciona;

    public JOELtestEspais(String paraula, boolean funciona) {
	this.paraula = paraula;
	this.funciona = funciona;
    }

    @Parameters
    public static Iterable<Object[]> nomUsuari() {
	return Arrays.asList(new Object[][] { { "marc lomas", false}, { " marclomas", false}, { "marclomas", true}, 
	    		{ "Marc_Lomas1754", true}, { "dsiomjm ", false}, { "18QW", true} });

    }
    

    @Test

    public void testEspais() {
	boolean noespais = programa.comprovarEspais(paraula);
	assertEquals(funciona, noespais);
//	fail("Not yet implemented");
    }

}
