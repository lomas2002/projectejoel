import static org.junit.Assert.*;
import java.util.Arrays;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)

public class JOELtestPreguntarContra {
    
    private String nom;
    private String esIgual;

    public JOELtestPreguntarContra(String nom, String esIgual) {
	this.nom = nom;
	this.esIgual = esIgual;
    }

    @Parameters
    public static Iterable<Object[]> nomUsuari() {
	return Arrays.asList(new Object[][] { { "marclomas", "Pass24-"}, { "marc", "Aquest nom d'usuari no existeix."}
				, { "LomasMarc", "Contra_123"}, { "Contra_123", "Aquest nom d'usuari no existeix."} });

    }
    

    @Test

    public void testPreguntaContra() {
	programa.usuari.add("marclomas");
        programa.usuari.add("Pass24-");
	programa.usuari.add("usuari123");
        programa.usuari.add("Password789#");
	programa.usuari.add("LomasMarc");
        programa.usuari.add("Contra_123");
	programa.preguntaContra(nom);
	equals(esIgual);
//	fail("Not yet implemented");
    }

}
