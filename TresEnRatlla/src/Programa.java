import java.util.ArrayList;
import java.util.Scanner;

public class Programa {
    static Scanner lector = new Scanner(System.in);
    static int opcio = 0;
    public static void main(String[] args) {
	//Aqu s far el men desde on es cridara als mtodes.
	// TODO Auto-generated method stub
	ArrayList<String> jugadorspartida = null;
	int[] veure = null;
	int pos1 = 0;
	int pos2 = 0;
	String jugador1 = "";
	String jugador2 = "";
	do {
	    PresentaMenu();
	    RespostaMenu();
	    switch (opcio) {
	    case 1:
		Ajuda.Ajuda();
		break;
	    case 2:
		jugador1 = Jugadors.PrimerJugador();
		jugador2 = Jugadors.SegonJugador();
		jugadorspartida = Jugadors.Definir(jugador1, jugador2);
		pos1 = jugadorspartida.indexOf(jugador1);
		pos2 = jugadorspartida.indexOf(jugador2);
		break;
	    case 3:
		Partida.comen�ar();
		Partida.imprimir();
		Partida.jugar(jugadorspartida, pos1, pos2);
		break;
	    case 4:
		veure = Partida.guanyar();
		VeureJugadors.Veure(veure, jugadorspartida);
		break;
	    case 5:
		System.out.println("Bona nit!");
		break;
	    default:
		System.out.println("Opci incorrecte");
	    }
	} while (opcio != 5);
    }
    
    static void PresentaMenu() {
	System.out.println("1. Mostrar ajuda.");
	System.out.println("2. Definir jugadors.");
	System.out.println("3. Jugar partida.");
	System.out.println("4. Veure jugadors.");
	System.out.println("5. Sortir.");
    }

    static int RespostaMenu() {
	boolean valid = false;

	do {
	    try {
		opcio = lector.nextInt();
		valid = true;
	    } catch (Exception e) {
		System.out.println("Atenci! nicament es permet insertar nmeros.");
		lector.next();
	    }
	} while (!valid);
	return opcio;
    }
    
    
}
